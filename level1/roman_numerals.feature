
Feature: convert a single digit
When a single roman numeral digit is given,
the converter should return the correct decimal value

Scenario: convert the number one
Given we need to convert a single roman numeral to a decimal value
When the roman numeral is I
Then the decimal value is 1
