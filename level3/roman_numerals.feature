
Feature: convert a single digit
When a single roman numeral digit is given,
the converter should return the correct decimal value

Scenario Outline: convert a single digit
Given we need to convert a single roman numeral to a decimal value
When the roman numeral is <roman_numeral>
Then the decimal value is <decimal_value>

Examples: Single Digits
   | roman_numeral | decimal_value |
   | I             | 1             |
   | V             | 5             |
   | X             | 10            |
   | L             | 50            |
   | C             | 100           |
   | M             | 1000          |

