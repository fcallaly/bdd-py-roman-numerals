class RomanNumeralConverter:

    def convert(self, roman_numeral):     
        SINGLE_DIGITS = {'I': 1, 'V': 5, 'X': 10,
                         'L': 50, 'C': 100, 'M': 1000}
 
        decimal_val = None

        if roman_numeral in SINGLE_DIGITS:
            decimal_val = SINGLE_DIGITS[roman_numeral]
        
        print("Converted", roman_numeral, "to", decimal_val)
        return decimal_val
