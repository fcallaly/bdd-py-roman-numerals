from behave import *
from roman_numerals import RomanNumeralConverter


@given(u'we need to convert a single roman numeral to a decimal value')
def step_impl(context):
    pass


@when(u'the roman numeral is {roman_numeral:w}')
def step_impl(context, roman_numeral):
    converter = RomanNumeralConverter()
    context.decimal_val = converter.convert(roman_numeral)


@then(u'the decimal value is {decimal_val:d}')
def step_impl(context, decimal_val):
    assert context.decimal_val is decimal_val

