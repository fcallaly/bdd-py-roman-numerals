class RomanNumeralConverter:

    def convert(self, roman_numeral):
        decimal_val = 1
        
        if roman_numeral == 'V':
            decimal_val = 5
        
        return decimal_val

